#!/usr/bin/python3.8

import argparse
import re

from matplotlib import pyplot as plt
import numpy as np

class PointBrowser():
    """
    Click on a point to select and highlight it -- the data that
    generated the point will be shown in the lower axes.  Use the 'n'
    and 'p' keys to browse through the next and previous points
    """

    def __init__(self):
        fig.canvas.mpl_connect('button_press_event', self.clean_lines)
        self.lastind = 0
        
        self.text = ax.text(0.05, 0.95, 'Threshold: None',
                            transform=ax.transAxes, va='top')
        self.selected, = ax.plot([xs[0]], [ys[0]], 'o', ms=12, alpha=0.4,
                                 color='yellow', visible=False)
        self.misses, = ax.plot([xs[0]], [ys[0]], linewidth=1.5, 
                                 color='black', visible=False)
        self.hits, = ax.plot([xs[0]], [ys[0]], linewidth=1.5,
                                 color='black', visible=False)
        self.correct_rejections, = ax.plot([xs[0]], [ys[0]], linewidth=1.5, 
                                 color='black', visible=False)
        self.false_alarms, = ax.plot([xs[0]], [ys[0]], linewidth=1.5,
                                 color='black', visible=False)
    # in the class body:
    def clean_lines(self, event):
        """
        Clean the lines if clicked outside the plot area. 

        Parameters: 
            event (no idea, gotta read more about mpl):
        """
        if event.inaxes is None:  # event.inaxes returns None if you click outside of the plot area, i.e. in the grey part surrounding the axes
            self.misses.set_visible(False)
            self.hits.set_visible(False)
            self.correct_rejections.set_visible(False)
            self.false_alarms.set_visible(False)
            event.canvas.draw()
            
    def calculate_intersection(self, point0, point1, point2, point3):
        """
        Calculate where two lines intersect.

        Returns:
            intersection_point (list, float): Coordinates of the intersection
        """
        x0, y0 = point0
        x1, y1 = point1
        x2, y2 = point2
        x3, y3 = point3

        try:
            px = ( (x0*y1-y0*x1)*(x2-x3)-(x0-x1)*(x2*y3-y2*x3) ) / ( (x0-x1)*(y2-y3)-(y0-y1)*(x2-x3) )
            py = ( (x0*y1-y0*x1)*(y2-y3)-(y0-y1)*(x2*y3-y2*x3) ) / ( (x0-x1)*(y2-y3)-(y0-y1)*(x2-x3) )
        except ZeroDivisionError:
            px = None
            py = None
        return [px, py]

    def onpress(self, event):
        if self.lastind is None:
            return
        if event.key not in ('n', 'p'):
            return
        if event.key == 'n':
            inc = 1
        else:
            inc = -1

        self.lastind += inc
        self.lastind = np.clip(self.lastind, 0, len(xs) - 1)
        self.update()

    def onpick(self, event):

        if event.artist != line:
            return True

        N = len(event.ind)
        if not N:
            return True

        # the click locations
        x = event.mouseevent.xdata
        y = event.mouseevent.ydata
        distances = np.hypot(x - xs[int(event.ind)], y - ys[int(event.ind)])
        indmin = distances.argmin()
        dataind = event.ind[indmin]

        self.lastind = dataind
        self.update()

    def update(self):
        """
        Update the canvas when a new point is selected.

        Returns:
            None: if lastind is None (?).
        """
        if self.lastind is None:
            return
        #Selected point
        dataind = self.lastind
        x_selected = xs[dataind]
        y_selected = ys[dataind]
        self.selected.set_visible(True)
        self.selected.set_data(x_selected, y_selected)
        
        ###I think this section could be made into a function.
        # #Misses
        line_visibility = True
        intersection = self.calculate_intersection([x_selected, y_selected],
                                            [x_selected, p],
                                            [0, 0],
                                            [p, p])
        self.misses.set_visible(line_visibility)
        x_values_miss = [x_selected, x_selected]
        if not intersection[0] is None:
            y_values_miss = [y_selected, intersection[0]]
            if intersection[-1] >= p:
                y_values_miss[-1] = p
        else:
            y_values_miss = y_selected
        self.misses.set_data(x_values_miss, y_values_miss)
        #Hits
        intersection = self.calculate_intersection([x_selected, y_selected ],
                                                [x_selected, 0],
                                            [q, 0],
                                            [q+p, p])
        x_values_hits = [x_selected, x_selected]
        if not intersection[1] is None:
            y_values_hits = [y_selected, intersection[1]]
        else:
            y_values_hits = [y_selected, 0]
        self.hits.set_visible(line_visibility)
        self.hits.set_data(x_values_hits, y_values_hits)
        #Correct rejections
        intersection = self.calculate_intersection([x_selected, y_selected],
                                                [q+p, y_selected],
                                                [q, 0],
                                                [q+p, p])
        if intersection[0] is not None:
            x_values_correct_rejections = [x_selected, intersection[0]]
        else:
            x_values_correct_rejections = [x_selected, p+q]
        y_values_correct_rejections = [y_selected, y_selected]
        self.correct_rejections.set_visible(line_visibility)
        self.correct_rejections.set_data(x_values_correct_rejections,
                                         y_values_correct_rejections)
        #False alarms
        intersection = self.calculate_intersection([x_selected, y_selected],
                                        [0, y_selected],
                                        [0, 0],
                                        [p, p])
        if intersection[0] is not None:
            x_values_false_alarms = [x_selected, intersection[0]]
        else:
            x_values_false_alarms = [x_selected, p]
        y_values_false_alarms = [y_selected, y_selected]
        self.false_alarms.set_visible(line_visibility)
        self.false_alarms.set_data(x_values_false_alarms,
                                   y_values_false_alarms)
        ###
        try:
            value = y_values_miss[-1]
        except TypeError:
            value = p
        text = [
        f"Threshold: {y_selected: .2f}",
        f"Hits (TP): {y_selected: .2f}",
        f"Misses (FN): {value - y_selected}", 
        f"False Alarms (FP): {x_selected - x_values_false_alarms[-1]: .2f} ",  
        f"Correct Rejections (TN): {x_values_correct_rejections[-1] - x_selected: .2f}"
        ]
        self.text.set_text("\n".join(text))
        
        fig.canvas.draw()
def caculate_rates(actives, compounds):
    """
    Calculate True Positive and False Positive.
    """
    tpf = [0]  # true positives found
    fpf = [0]  # false positives found

    foundactives = 0
    founddecoys = 0
    for compound_score in compounds:
        if compound_score[0] in actives:
            foundactives += 1
        else:
            founddecoys += 1
        tpf.append(foundactives)
        fpf.append(founddecoys)
    tpr = list(map(lambda x: float(x)/foundactives, tpf))
    fpr = list(map(lambda x: float(x)/founddecoys, fpf))

    return tpr, fpr

def get_actives_regex(fname, active_regex):
    """
    Get active names from scores file by regex (case insensitive!).

    Parameters:
    fname (str): Name of the file containing the scores.
    active_regex (str): Regex for matching actives.

    Returns:
    actives (list, str): List containing the name of the actives.
    """
    active_regex_comp = re.compile(active_regex, re.IGNORECASE)
    actives = []
    with open(fname, 'r') as f:
        for line in f:
            clean_line = re.split(r'[\s,;]', line)[0].strip()
            if active_regex_comp.match(clean_line):
                actives.append(clean_line)
    return actives
# def get_compounds_numbers(fname, active_regex):
#     """
#     Get numbers of actives and total compounds.

#     Parameters: 
#         fname (str): Name of the file containing the scores.
#         active_regex (str): Regex for matching actives.

#     Returns:
#         data (tuple, int, int): Number of actives compounds and total.
#     """
#     active_number = len(get_actives_regex(fname, active_regex))
#     total = len(open(fname, 'r').readlines())
#     return active_number, total

if __name__ == '__main__':
    ##Argument parser block
    parser = argparse.ArgumentParser(description="Pytoc: TOC curve plotter.")
    
    parser.add_argument(
        "-sc", "--scores", dest='score_file', metavar='scores.csv',
        type = str, default=None, help="Specify a score file."
        )
    parser.add_argument(
        "-an", "--activename", dest= 'active_regex', metavar='active_regex',
        type=str, default=None, help="Specify regex for name of active ligands (case insensitive!)."
        )
    parser_args = parser.parse_args()

    score_file = parser_args.score_file
    active_regex = parser_args.active_regex
    total_compounds = []
    with open(score_file, "r") as f:
        for line in f:
            line = line.rstrip()
            clean_line = tuple(re.split(r'[ ,;]', line))
            total_compounds.append(clean_line)
    actives = get_actives_regex(score_file, active_regex)
    tpr, fpr = caculate_rates(actives, total_compounds)
    p = len(actives)
    q = len(total_compounds) - p 
    x_toc = []
    y_toc = []
    for tpri, fpri in zip(tpr, fpr):
        x_toc.append((p*tpri + q*fpri))
        y_toc.append(p*tpri)
    xs = x_toc #esto es un asco
    ys = y_toc #esto es un asco
    fig, ax = plt.subplots()
    
    #Curva TOC
    line, = ax.plot(x_toc, y_toc, '-b') #o: Puntos, -:lineas, b: blue
    ax.scatter(x_toc, y_toc, s=5)
    ## Paralelogramo que delimita el espacio TOC
    # Recta positiva (izquierda)
    punto0 = [p, p]
    punto1 = [q, 0]
    punto2 = [q+p, p]
    x_values_positivo = [0, punto0[0]]
    y_values_positivo = [0, punto0[1]]
    #Recta negativa (derecha)
    x_values_negativo = [punto1[0], punto2[0]]
    y_values_negativo = [punto1[1], punto2[1]]
    #Techo
    x_values_techo = [punto0[0], punto2[0]]
    y_values_techo = punto0
    #Piso
    x_values_piso = [0, punto1[0]]
    y_values_piso = [0, 0]
    plt.plot(x_values_positivo, y_values_positivo,
            x_values_negativo, y_values_negativo,
            x_values_techo, y_values_techo, 
            x_values_piso, y_values_piso,
            linestyle='dashed', color='black', linewidth=2)
    ##Curva random
    x_values_random = [0, punto2[0]]
    y_values_random = [0, punto2[1]]
    plt.plot(x_values_random, y_values_random,
             linestyle='dashed', color='red', label='random')
    ##Etiqueta
    plt.xlabel("Hits + False Alarms")
    plt.ylabel("Hits")
    ##Limites
    plt.xlim(0, p+q)
    plt.ylim(0, p)
    ##Titulo
    plt.title("TOC CURVE")
    ##Posicion de la leyenda "random"
    plt.legend(loc=4)
    ##Este pedazo activa el picker y conecta con el pointbrowser.
    line.set_picker(True)
    line.set_pickradius(0.5) #era 5
    ##Este bloque tiene que revisarse y cauclar lo multiplicadores en base
    #a la cantidad de activos y compuestos detectados.
    plt.gca().xaxis.set_major_locator(plt.MultipleLocator(500)) #era 1
    plt.gca().yaxis.set_major_locator(plt.MultipleLocator(10)) #era 1
    ##
    browser = PointBrowser()
    fig.canvas.mpl_connect('pick_event', browser.onpick)
    fig.canvas.mpl_connect('key_press_event', browser.onpress)
    ##Finalmente, que muestre todo.
    plt.show()
    